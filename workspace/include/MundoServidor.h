//Ignacio Botella
// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"
//#include "listaEsfera.h"


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
//#include "Raqueta.h"
#include "DatosMemCompartida.h"
#include <pthread.h>
class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();
	void RecibeComandosJugador();	

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	//listaEsfera listado;
	int mipipe;
	int tuberiacs;
	int tuberiateclas;
	int puntos1;
	int puntos2;
	char buffer1[128]="Jugador 1 marca 1 punto, lleva un total de 0 puntos.";
	char buffer2[128]="Jugador 2 marca 1 punto, lleva un total de 0 puntos.";
	DatosMemCompartida *datosm;
	DatosMemCompartida datos;
	char *org;
	pthread_t thid1;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
