// Esfera.cpp: implementation of the Esfera class.
//Ignacio Botella Larrubia
//////////////////////////////////////////////////////////////////////

#include "listaEsfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

listaEsfera::listaEsfera(){
	numero=0;
	for int i=0;i<10;i++)lista[i]=0;
}

bool listaEsfera::agregar(Esfera *e){
	for (int i=0;i<numero;i++)if (lista[i]==e)return false;
	if (numero<10) lista[numero++]=e;
	else return false;
	return true;
}
