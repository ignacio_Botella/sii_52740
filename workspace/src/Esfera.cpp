// Esfera.cpp: implementation of the Esfera class.
//Ignacio Botella Larrubia
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
}
Esfera::Esfera(Vector2D cen,Vector2D vel){
	centro=cen;
	velocidad.x=vel.x/2;
	velocidad.y=vel.y;
}
Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	centro= centro + velocidad * t;	//supongo velocidad constante
}
